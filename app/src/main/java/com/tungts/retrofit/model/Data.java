package com.tungts.retrofit.model;

/**
 * Created by tungts on 7/23/2017.
 */

public class Data {

    private User data;

    public User getData() {
        return data;
    }

    public void setData(User data) {
        this.data = data;
    }
}

package com.tungts.retrofit.api;

import com.tungts.retrofit.model.Data;
import com.tungts.retrofit.model.Page;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by tungts on 7/23/2017.
 */

public interface UserAPI {

    @GET("/api/users/2")
    Call<Data> getSingleUser();

    @GET("/api/users/{id}")
    Call<Data> getSingleUserById(@Path("id") int id);

    //get List User on page
    @GET("/api/users")
    Call<Page> getListUserOnPage(@Query("page") int page);

//     post object to API
//    @POST("/api/users")
//    Call<Users> postUsers(@Body Users users);

    //Post data
    @FormUrlEncoded
    @POST("/api/users")
    Call<ResponseBody> postUsers(@Field("name") String name,@Field("job") String job);

//    Put object to API
//    @PUT("/api/users/2")
//    Call<Users> putUsers(@Body Users users);

    //Put data by two way
    @Multipart
    @PUT("/api/users/2")
    Call<ResponseBody> putUserByPart(@Part("name") String name, @Part("job") String job);

    @FormUrlEncoded
    @PUT("/api/users/2")
    Call<ResponseBody> putUserByField(@Field("name") String name, @Field("job") String job);
}

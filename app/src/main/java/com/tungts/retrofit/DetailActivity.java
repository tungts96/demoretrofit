package com.tungts.retrofit;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;
import com.tungts.retrofit.api.UserAPI;
import com.tungts.retrofit.api.UserAPIHelper;
import com.tungts.retrofit.model.Data;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.tungts.retrofit.api.UserAPIHelper.BASE_URL;

public class DetailActivity extends AppCompatActivity {

    ImageView imgDetail;
    TextView tvId, tvFirstName, tvLastName;

    UserAPI userAPI;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        imgDetail = (ImageView) findViewById(R.id.imgDetail);
        tvId = (TextView) findViewById(R.id.tvIdDetail);
        tvFirstName = (TextView) findViewById(R.id.tvFirstNameDetail);
        tvLastName = (TextView) findViewById(R.id.tvLastNameDetail);
        progressDialog = new ProgressDialog(this);

        userAPI = UserAPIHelper.createUserAPI();
        int id = getIntent().getIntExtra("ST", 0);
        if (id != 0) {
            progressDialog.setMessage("Loading");
            progressDialog.show();
            userAPI.getSingleUserById(id).enqueue(callbackData);
        }
    }

    Callback<Data> callbackData = new Callback<Data>() {
        @Override
        public void onResponse(Call<Data> call, Response<Data> response) {
            if (response.isSuccessful()) {
                progressDialog.cancel();
                if (response.body() == null) {
                    showToast("Không có dữ liệu thỏa mãn Id");
                } else {
                    Data data = response.body();
                    Picasso.with(DetailActivity.this).load(data.getData().getAvatar()).into(imgDetail);
                    tvId.setText(String.format(getString(R.string.id),data.getData().getId()));
                    tvFirstName.setText(String.format(getString(R.string.first_name),data.getData().getFirst_name()));
                    tvLastName.setText(String.format(getString(R.string.last_name),data.getData().getLast_name()));
                }
            } else {
                progressDialog.cancel();
                showToast("Không có dữ liệu thỏa mãn Id");
                tvId.setText("Id: ");
                tvFirstName.setText("First Name: ");
                tvLastName.setText("Last Name: ");
                imgDetail.setImageResource(R.mipmap.ic_launcher);
            }
        }

        @Override
        public void onFailure(Call<Data> call, Throwable t) {
            progressDialog.cancel();
            showToast(t.getLocalizedMessage());
        }
    };

    private void showToast(String s) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }
}

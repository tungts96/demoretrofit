package com.tungts.retrofit;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tungts.retrofit.adapter.UserAdapter;
import com.tungts.retrofit.api.UserAPI;
import com.tungts.retrofit.interfaces.OnLoadMoreListener;
import com.tungts.retrofit.interfaces.OnRecycleViewItemClick;
import com.tungts.retrofit.model.Page;
import com.tungts.retrofit.model.User;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.tungts.retrofit.api.UserAPIHelper.BASE_URL;
import static com.tungts.retrofit.api.UserAPIHelper.createUserAPI;

/**
 * Created by tungts on 7/24/2017.
 */

public class ListUserActivity extends AppCompatActivity implements View.OnClickListener{

    EditText edtPage;
    Button btnNext, btnPrevious;

    UserAPI userAPI;
    ProgressDialog progressDialog;

    int total_page;
    int current_page = 1;

    RecyclerView rcvUser;
    UserAdapter userAdapter;
    ArrayList list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_user);
        addControls();
        addEvents();
        userAPI = createUserAPI();
        progressDialog = new ProgressDialog(this);
        progressDialog.show();
        progressDialog.setMessage("Loading");
        userAPI.getListUserOnPage(current_page).enqueue(pageCallback);
    }


    private void addEvents() {
        btnNext.setOnClickListener(this);
        btnPrevious.setOnClickListener(this);
        loadMore();
        recycleViewItemClick();
    }

    private void recycleViewItemClick() {
        userAdapter.setOnRecycleViewItemClick(new OnRecycleViewItemClick() {
            @Override
            public void itemClick(int positon) {
                User user = (User) list.get(positon);
                Intent intent = new Intent(ListUserActivity.this,DetailActivity.class);
                intent.putExtra("ST",user.getId());
                startActivity(intent);
            }
        });
    }

    private void loadMore() {
        userAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (current_page < total_page){
                    list.add(null);
                    userAdapter.notifyDataSetChanged();
                    current_page+=1;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            userAPI.getListUserOnPage(current_page).enqueue(pageCallback);
                        }
                    },3000);
                }
            }
        });
    }

    private void addControls() {
        edtPage = (EditText) findViewById(R.id.edtPage);
        btnNext = (Button) findViewById(R.id.btnNext);
        btnPrevious = (Button) findViewById(R.id.btnPre);
        rcvUser = (RecyclerView) findViewById(R.id.rcvUser);
        list = new ArrayList();
        userAdapter = new UserAdapter(this,list,rcvUser);
        rcvUser.setLayoutManager(new LinearLayoutManager(this));
        rcvUser.setAdapter(userAdapter);
    }


    Callback<Page> pageCallback = new Callback<Page>() {
        @Override
        public void onResponse(Call<Page> call, Response<Page> response) {
            if (response.isSuccessful()){
                Page page = response.body();
                total_page = page.getTotal_pages();
                edtPage.setText(""+current_page+"\\"+total_page);
                if (current_page == 1 ){
                    list.addAll(page.getData());
                    current_page=2;
                    userAPI.getListUserOnPage(current_page).enqueue(pageCallback);
                } else if (current_page == 2 ){
                    if (progressDialog.isShowing()){
                        progressDialog.cancel();
                    }
                    list.addAll(page.getData());
                    userAdapter.notifyDataSetChanged();
                } else if (current_page > 2){
                    list.remove(list.size()-1);
                    userAdapter.notifiDataLoadmore(page.getData());
                    userAdapter.setLoading();
                }
            } else {
                try {
                    showToast(response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<Page> call, Throwable t) {
            progressDialog.cancel();
            showToast(t.getLocalizedMessage());
        }
    };

    private void showToast(String s){
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.btnNext:
//                if (current_page<total_page){
//                    current_page+=1;
//                } else {
//                    current_page=1;
//                }
//                progressDialog.show();
//                userAPI.getListUserOnPage(current_page).enqueue(pageCallback);
                break;
            case R.id.btnPre:
//                if (current_page >1){
//                    current_page-=1;
//                    progressDialog.show();
//                    userAPI.getListUserOnPage(current_page).enqueue(pageCallback);
//                }
                break;
        }
    }

}

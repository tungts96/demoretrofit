package com.tungts.retrofit;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tungts.retrofit.api.UserAPI;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.tungts.retrofit.api.UserAPIHelper.BASE_URL;
import static com.tungts.retrofit.api.UserAPIHelper.createUserAPI;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnGetUserById,btnGetListUserOnPage;

    EditText edtPostName, edtPostJob;
    Button btnPost;

    EditText edtPutName, edtPutJob;
    Button btnPut;

    UserAPI userAPI;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addControlls();
        addEvents();
        userAPI = createUserAPI();
        progressDialog = new ProgressDialog(this);
    }


    private void addEvents() {
        btnGetUserById.setOnClickListener(this);
        btnGetListUserOnPage.setOnClickListener(this);
        btnPut.setOnClickListener(this);
        btnPost.setOnClickListener(this);
    }

    private void addControlls() {
        btnGetUserById = (Button) findViewById(R.id.btnGetUserById);
        btnGetListUserOnPage = (Button) findViewById(R.id.btnGetListUserOnPage);
        edtPostName = (EditText) findViewById(R.id.edtPostName);
        edtPostJob = (EditText) findViewById(R.id.edtPostJob);
        btnPost = (Button) findViewById(R.id.btnPost);
        edtPutName = (EditText) findViewById(R.id.edtPutName);
        edtPutJob = (EditText) findViewById(R.id.edtPutJob);
        btnPut = (Button) findViewById(R.id.btnPut);
    }

    Callback<ResponseBody> callBackPostResponeBody = new Callback<ResponseBody>() {
        @Override
        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
            if (response.isSuccessful()){
                progressDialog.cancel();
                try {
                    showToast("Respone: " + response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                progressDialog.cancel();
                showToast("Post fault");
            }
        }

        @Override
        public void onFailure(Call<ResponseBody> call, Throwable t) {
            progressDialog.cancel();
            showToast("On Failure: "+ t.getLocalizedMessage());
        }
    };

    Callback<ResponseBody> callBackPutResponeBody = new Callback<ResponseBody>() {
        @Override
        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
            if (response.isSuccessful()){
                progressDialog.cancel();
                try {
                    showToast("Respone: " + response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                progressDialog.cancel();
                showToast("Put fault");
            }
        }

        @Override
        public void onFailure(Call<ResponseBody> call, Throwable t) {
            progressDialog.cancel();
            showToast("On Failure: "+ t.getLocalizedMessage());
        }
    };


    private void showToast(String s){
        Toast.makeText(MainActivity.this, s, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.btnGetUserById:
                Intent intent = new Intent(MainActivity.this, SingleUserActivity.class);
                startActivity(intent);
                break;
            case R.id.btnGetListUserOnPage:
                Intent intent1 = new Intent(MainActivity.this, ListUserActivity.class);
                startActivity(intent1);
                break;
            case R.id.btnPost:
                progressDialog.setMessage("Loading");
                progressDialog.show();
                String postName = edtPostName.getText().toString();
                String postJob = edtPostJob.getText().toString();
                userAPI.postUsers(postName,postJob).enqueue(callBackPostResponeBody);
                break;
            case R.id.btnPut:
                progressDialog.setMessage("Loading");
                progressDialog.show();
                String putName = edtPutName.getText().toString();
                String putJob = edtPutJob.getText().toString();
                userAPI.putUserByField(putName,putJob).enqueue(callBackPutResponeBody);
                break;
        }
    }

}

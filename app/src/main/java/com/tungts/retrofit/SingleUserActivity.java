package com.tungts.retrofit;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;
import com.tungts.retrofit.api.UserAPI;
import com.tungts.retrofit.model.Data;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.tungts.retrofit.api.UserAPIHelper.BASE_URL;
import static com.tungts.retrofit.api.UserAPIHelper.createUserAPI;

public class SingleUserActivity extends AppCompatActivity implements View.OnClickListener{

    EditText edtQueryId;
    Button btnSearch;

    TextView tvId,tvFirstName,tvLastName;
    ImageView imgProfileUser;

    UserAPI userAPI;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_user);
        addControls();
        addEvents();
        userAPI = createUserAPI();
        progressDialog = new ProgressDialog(this);
    }

    private void addEvents() {
        btnSearch.setOnClickListener(this);
    }

    private void addControls() {
        edtQueryId = (EditText) findViewById(R.id.edtId);
        btnSearch = (Button) findViewById(R.id.btnGetUser);
        tvId = (TextView) findViewById(R.id.tvIdUser);
        tvFirstName = (TextView) findViewById(R.id.tvFirstNameUser);
        tvLastName = (TextView) findViewById(R.id.tvLastNameUser);
        imgProfileUser = (ImageView) findViewById(R.id.imgUser);
    }

    Callback<Data> callbackData = new Callback<Data>() {
        @Override
        public void onResponse(Call<Data> call, Response<Data> response) {
            if (response.isSuccessful()){
                progressDialog.cancel();
                if (response.body() == null){
                    showToast("Không có dữ liệu thỏa mãn Id");
                } else  {
                    Data data = response.body();
                    //tvId.setText("Id: "+data.getData().getId()+"");
//                    String formattedString = getString(R.string.string_to_format,5f , 5, "ttt",4);
                    tvId.setText(String.format(getString(R.string.id),data.getData().getId()));
                    tvFirstName.setText(String.format(getString(R.string.first_name),data.getData().getFirst_name()));
                    tvLastName.setText(String.format(getString(R.string.last_name),data.getData().getLast_name()));
                    Picasso.with(SingleUserActivity.this).load(data.getData().getAvatar()).into(imgProfileUser);
                }
            } else {
                progressDialog.cancel();
                showToast("Không có dữ liệu thỏa mãn Id");
                tvId.setText("Id: ");
                tvFirstName.setText("First Name: ");
                tvLastName.setText("Last Name: ");
                imgProfileUser.setImageResource(R.mipmap.ic_launcher);
            }
        }

        @Override
        public void onFailure(Call<Data> call, Throwable t) {
            progressDialog.cancel();
            showToast(t.getLocalizedMessage());
        }
    };

    private void showToast(String s){
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.btnGetUser){
            progressDialog.show();
            if (edtQueryId.getText().toString().length() >0 ){
                int idUser = Integer.parseInt(edtQueryId.getText().toString());
                userAPI.getSingleUserById(idUser).enqueue(callbackData);
            }
        }
    }
}

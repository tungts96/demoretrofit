package com.tungts.retrofit.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tungts.retrofit.interfaces.OnLoadMoreListener;
import com.tungts.retrofit.R;
import com.tungts.retrofit.interfaces.OnRecycleViewItemClick;
import com.tungts.retrofit.model.User;

import java.util.List;

/**
 * Created by tungts on 7/24/2017.
 */

public class UserAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    public static final int ITEM_USER = 99;
    public static final int ITEM_LOADING = 100;

    private Context context;
    private List list;
    private LayoutInflater layoutInflater;

    private RecyclerView recyclerView;
    private OnLoadMoreListener onLoadMoreListener;
    private boolean isLoading;

    private OnRecycleViewItemClick onRecycleViewItemClick;

    public UserAdapter(Context context, List list, RecyclerView recyclerView) {
        this.context = context;
        this.list = list;
        Log.e("SonTung",list.size()+"");
        layoutInflater = LayoutInflater.from(context);
        this.recyclerView = recyclerView;
        loadMore();
    }

    private void loadMore() {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                int totalItemCount = linearLayoutManager.getItemCount();
                int lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (lastVisibleItem == (totalItemCount -1) && !isLoading){
                    if (onLoadMoreListener != null){
                        onLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    public void setLoading(){
        isLoading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public void setOnRecycleViewItemClick(OnRecycleViewItemClick onRecycleViewItemClick){
        this.onRecycleViewItemClick = onRecycleViewItemClick;
    }

    public void notifiDataLoadmore(List list){
//        this.list.clear();
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ITEM_USER) {
            View view = layoutInflater.inflate(R.layout.item_user,parent,false);
            Log.e("SonTung","create");
            return new UserViewHolder(view);
        } else {
            View view = layoutInflater.inflate(R.layout.item_loading,parent,false);
            return new LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof UserViewHolder){
            Log.e("SonTung","bling");
            User user = (User) list.get(position);
            ((UserViewHolder) holder).tvId.setText("Id: "+user.getId());
            ((UserViewHolder) holder).tvFirstName.setText("First Name: "+user.getFirst_name());
            ((UserViewHolder) holder).tvLastName.setText("Last Name: "+user.getLast_name());
            Picasso.with(context).load(user.getAvatar()).into(((UserViewHolder) holder).imgProfile);
        } else {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (list.get(position) instanceof User){
            Log.e("SonTung",position+"");
            return ITEM_USER;
        } else {
            return ITEM_LOADING;
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class UserViewHolder extends RecyclerView.ViewHolder{
        ImageView imgProfile;
        TextView tvId, tvFirstName, tvLastName;

        public UserViewHolder(View itemView) {
            super(itemView);
            imgProfile = itemView.findViewById(R.id.imgProfile);
            tvId = itemView.findViewById(R.id.tvId);
            tvFirstName = itemView.findViewById(R.id.tvFirstName);
            tvLastName = itemView.findViewById(R.id.tvLastName);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onRecycleViewItemClick != null){
                        onRecycleViewItemClick.itemClick(getAdapterPosition());
                    }
                }
            });
        }
    }

    class LoadingViewHolder extends RecyclerView.ViewHolder{

        ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressLoading);
        }
    }
}
